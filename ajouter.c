
#include <stdio.h>
#define T 5001
#define min(a,b) (a<=b?a:b)



struct personne {
  char prenom[21];
  char nom[27];
  char ville[23];
  char cp[6];
  char tel[15];
  char mail[47];
  char metier[18];
};

typedef struct personne Personne;

char *strsep(char **stringp, const char *delim);
Personne value_to_struc(char *str,Personne p);
void remplireTab(Personne *tab);

int aj(Personne client, Personne *tab, int *taille);

char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;

    s2=str; //this is here to avoid warning of assignment from incompatible pointer type

    s2 = strsep( &s2, "\n" ); //retire le \n

    while (s2!=0 ) {

            s1 = strsep( &s2, "," );
            //printf("%s\n",p.cp);

            switch (count){
                case 1:
                    strcpy(p.prenom ,s1);
                    break;
                case 2:
                    strcpy(p.nom ,s1);
                    break;
                case 3:
                    strcpy(p.ville ,s1);
                    break;
                case 4:
                    strcpy(p.cp ,s1);
                    break;
                case 5:
                    strcpy(p.tel ,s1);
                    break;
                case 6:
                    strcpy(p.mail ,s1);
                    break;
                case 7:
                    strcpy(p.metier ,s1);
                    break;
            }

            count +=1;
    }


    return p;
}

void remplireTab(Personne *tab){
    char nomfichier[8] = "an.csv";
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];
    int count = 0;

    FILE* entree;
    //printf("nom du fichier a lire: ");
    //scanf("%20s", nomfichier);

    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
            //puts(tab[count].metier);
            tab[count] = value_to_struc(&buffer,tab[count]);
            //puts(tab[count].metier);
            count++;
            if (count>T-1){
                fclose(entree);
            }

    }

    fclose(entree);



}



int aj(Personne client, Personne *tab, int *taille){
    realloc(tab, taille+1*sizeof(Personne));
    tab[*taille] = client;

    *taille = *taille +1;

}


int main(){
    int TT = 5001;

    Personne* tab = malloc(TT* sizeof(Personne));
    //Personne tab[T];


    remplireTab(tab);


    printf("%s\n",tab[TT-1].nom);


    Personne p1;

    strcpy(p1.prenom , "prenom");
    strcpy(p1.nom , "nom");
    strcpy(p1.ville, "ville");
    strcpy(p1.cp, "cp");
    strcpy(p1.tel, "tel");
    strcpy(p1.mail,"mail");



   if(aj(p1,tab,&TT)){

    printf("reussi\n");
   }


    printf("%s\n",tab[TT-1].nom);




printf("fini");
return 0;
}
