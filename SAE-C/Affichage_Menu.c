#include "Affichage_Menu.h"
#include <stdio.h>
#include <stdlib.h>

int Affichage_Menu()
{
    int choix;

    printf("=========================MENU=========================\n");
    printf("(1). Ajouter de nouveaux clients\n");
    printf("(2). Modifier les donnees sur un client\n");
    printf("(3). Suppression d'un client\n");
    printf("(4). Afficher la liste de tous les clients\n");
    printf("(5). Filtre\n");
    printf("(6). Recherche\n");
    printf("(7). Affichage des clients ayant des donnees manquant\n");
    printf("(8). Sauvegarde des donnees\n");
    printf("(0). Quitter\n");
    printf("======================================================\n\n");

    printf("Entre votre choix s'il-vous-plait : ");
    scanf("%d", &choix);

    return choix;
}
