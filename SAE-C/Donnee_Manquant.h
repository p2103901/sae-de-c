#ifndef Donnee_Manquant_h
#define Donnee_Manquant_h
#include <stdio.h>
#include <stdlib.h>
#include "Struc_Pres.h"
#include "Remp_Tab.h"

void Donnee_Manquant(Personne *tab, int T);

#endif // Donnee_Manquant_h
