#include <stdio.h>
#include <string.h>
#define min(a,b) (a<=b?a:b)
#define T 5001
struct personne {
  char prenom[22];
  char nom[27];
  char ville[23];
  char cp[5];
  char tel[14];
  char mail[48];
  char metier[18];
};

typedef struct personne Personne;


char *strsep(char **stringp, const char *delim);
Personne value_to_struc(char *str,Personne p);






char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;

    s2=str; //this is here to avoid warning of assignment from incompatible pointer type

    s2 = strsep( &s2, "\n" ); //retire le \n

    while (s2!=0 ) {
            while( *s2 == ' ' || *s2 == '\t' )  s2++;

            s1 = strsep( &s2, "," );
            puts(s1);

            switch (count){
                case 1: strcpy(p.prenom ,s1); break;
                case 2: strcpy(p.nom ,s1);break;
                case 3: strcpy(p.ville ,s1); break;
                case 4: strcpy(p.cp ,s1); break;
                case 5: strcpy(p.tel ,s1); break;
                case 6: strcpy(p.mail ,s1); break;
                case 7: strcpy(p.metier ,s1); break;
            }

            count +=1;
    }

    return p;
}



void remplireTab(Personne *tab);

void remplireTab(Personne *tab)
{
    char nomfichier[6];
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];
    int count = 0;

    FILE* entree;
    printf("nom du fichier a lire: ");
    scanf("%20s", nomfichier);
    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
            //puts(tab[count].metier);
            tab[count] = value_to_struc(&buffer,tab[count]);
            //puts(tab[count].metier);
            count++;

    }

    fclose(entree);



}


int main ()
{
    Personne tab[5000] ;
    remplireTab(tab);


    printf("nom:");
    puts(tab[0].cp);

    printf("prenom:");
    puts(tab[0].prenom);

    printf("nom:");
    puts(tab[1000].nom);

    printf("prenom:");
    puts(tab[100].mail);

    /*
    char str[] = "jean,bob,Lyon,69000,0755665555,p";

    tab[0] = value_to_struc(&str,tab[0]);

    puts(tab[0].metier);
    */


        return 0;
    }

