void afficherClient(Personne *tab)
{
    remplireTab(tab);
    int i, tranche;
    printf("=== Menu d'affichage des clients par tranches de 200 personnes ===\n");
    printf("Saisir une valeur entre 0 et 5200 : ");
    scanf("%d", &tranche);

    for(i=tranche;i<tranche+200;i++)
    {
        printf("===========================================================================\n");
        printf("Numero : %d  \n", i);
        printf("Prenom : %s  \n", tab[i].prenom);
        printf("Nom : %s  \n", tab[i].nom);
        printf("Ville : %s  \n", tab[i].ville);
        printf("Code Postal : %s  \n", tab[i].cp);
        printf("Telephone : %s  \n", tab[i].tel);
        printf("Mail : %s  \n", tab[i].mail);
        printf("Metier : %s  \n", tab[i].metier);
        printf("===========================================================================\n");
    }
}
