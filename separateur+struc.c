#include <stdio.h>
#include <string.h>
#define min(a,b) (a<=b?a:b)

struct personne {
  char prenom[50];
  char nom[50];
  char ville[50];
  char cp[5];
  char tel[14];
  char mail[70];
  char metier[50];
};

typedef struct personne Personne;


char *strsep(char **stringp, const char *delim);
Personne value_to_struc(char *str,Personne p);






char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;
    s2=str; //this is here to avoid warning of assignment from incompatible pointer type
    while (s2!=0 ) {
            while( *s2 == ' ' || *s2 == '\t' )  s2++;

            s1 = strsep( &s2, "," );

            switch (count){
                case 1: strcpy(p.prenom ,s1);
                case 2: strcpy(p.nom ,s1);
                case 3: strcpy(p.ville ,s1);
                case 4: strcpy(p.cp ,s1);
                case 5: strcpy(p.tel ,s1);
                case 6: strcpy(p.mail ,s1);
                case 7: strcpy(p.metier ,s1);
            }

            count +=1;
    }

    return p;
}


int main ()
{
    Personne p1 ;


    char str[] = ",Rey Le Lopes,PARIS,75017,01.64.64.36.15,,avocat";

    p1 = value_to_struc(&str,p1);

    puts(p1.metier);


        return 0;
    }
