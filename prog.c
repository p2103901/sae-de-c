#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#define min(a,b) (a<=b?a:b)

#define T 5001

struct personne {
  char prenom[30];
  char nom[50];
  char ville[50];
  char cp[5];
  char tel[10];
  char mail[30];
  char metier[20];
};

typedef struct personne Personne;

void viderBuffer()
{
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

char *strsep(char **stringp, const char *delim);
Personne value_to_struc(char *str,Personne p);
void remplireTab(Personne *tab);
void tri_fusion(int deb, int fin, Personne *tab);
void fusion(int deb,int mil, int fin, Personne *tab);
int choix_tri(int col, Personne A, Personne B);

int affichageMenu();
void affichageFonction(int res_affichageMenu);






char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;

    s2=str; //this is here to avoid warning of assignment from incompatible pointer type

    s2 = strsep( &s2, "\n" ); //retire le \n

    while (s2!=0 ) {
            while( *s2 == ' ' || *s2 == '\t' )  s2++;

            s1 = strsep( &s2, "," );

            switch (count){
                case 1: strcpy(p.prenom ,s1);
                case 2: strcpy(p.nom ,s1);
                case 3: strcpy(p.ville ,s1);
                case 4: strcpy(p.cp ,s1);
                case 5: strcpy(p.tel ,s1);
                case 6: strcpy(p.mail ,s1);
                case 7: strcpy(p.metier ,s1);
            }

            count +=1;
    }

    return p;
}





void remplireTab(Personne *tab)
{
    char nomfichier[8] = "an.csv";
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];
    int count = 0;

    FILE* entree;
    //printf("nom du fichier a lire: ");
    //scanf("%20s", nomfichier);

    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
            //puts(tab[count].metier);
            tab[count] = value_to_struc(&buffer,tab[count]);
            //puts(tab[count].metier);
            count++;
            if (count>T-1){
                fclose(entree);
            }


    }

    fclose(entree);



}

int choix_tri(int col, Personne A, Personne B){

switch (col){
case 1:
    return strcmpi(A.prenom, B.prenom);
case 2:
    return strcmpi(A.nom, B.nom);
case 3:
    return strcmpi(A.ville, B.ville);
case 4:
    return strcmpi(A.cp, B.cp);
case 5:
    return strcmpi(A.tel, B.tel);
case 6:
    return strcmpi(A.mail, B.mail);
case 7:
    return strcmpi(A.metier, B.metier);
default:
    return 0;
}


}


void fusion(int deb,int mil, int fin, Personne *tab){

    int tai1 = mil - deb +1;
    int tai2 = fin - mil;

    Personne A[tai1];
    Personne B[tai2];




    for(int i =0; i<tai1;i++)
        A[i] = tab[deb +i];
    for(int j=0;j<tai2;j++)
        B[j] = tab[mil +1 +j];


     int i, j, k;

    i =0;
    j = 0;
    k = deb;


    while(i< tai1 && j < tai2)
    {

        if (choix_tri(2, A[i],B[j])<=0){

            tab[k] = A[i];
            i++;
        }
        else{

            tab[k] = B[j];

            j++;
        }
        k++;
    }
    while (i < tai1)
    {
        tab[k] = A[i];
        i++;
        k++;
    }

    while (j < tai2)
    {
        tab[k] = B[j];
        j++;
        k++;
    }




}


void tri_fusion(int deb, int fin, Personne *tab){

      if (deb < fin){

        int mil = (deb+fin)/2;

        tri_fusion(deb, mil ,tab);

        tri_fusion(mil + 1, fin ,tab);

        fusion(deb, mil, fin, tab);



      }






}

void afficherClient(Personne *tab)
{
    remplireTab(tab);
    int i, tranche;
    printf("=== Menu d'affichage des clients par tranches de 200 personnes ===\n");
    printf("Saisir une valeur entre 0 et 5200 : ");
    scanf("%d", &tranche);

    for(i=tranche;i<tranche+200;i++)
    {
        printf("===========================================================================\n");
        printf("Numero : %d  \n", i);
        printf("Prenom : %s  \n", tab[i].prenom);
        printf("Nom : %s  \n", tab[i].nom);
        printf("Ville : %s  \n", tab[i].ville);
        printf("Code Postal : %s  \n", tab[i].cp);
        printf("Telephone : %s  \n", tab[i].tel);
        printf("Mail : %s  \n", tab[i].mail);
        printf("Metier : %s  \n", tab[i].metier);
        printf("===========================================================================\n");
    }
}

void donneeManquant(Personne *tab)
{
    remplireTab(tab);
    int i, tranche;
    int null = strlen(tab[0].prenom);
    printf("=== Menu d'affichage des clients avec des donnees manquantes par tranches de 200 personnes ===\n");
    printf("Saisir une valeur entre 0 et 5200 : ");
    scanf("%d", &tranche);
    for(i=tranche; i<tranche+200; i++)
    {
        if(strlen(tab[i].prenom) == null || strlen(tab[i].nom) == null || strlen(tab[i].ville) == null || strlen(tab[i].cp) == null || strlen(tab[i].tel) == null || strlen(tab[i].mail) == null || strlen(tab[i].metier) == null)
        {
            printf("===========================================================================\n");
            printf("Numero : %d  \n", i);
            printf("Prenom : %s  \n", tab[i].prenom);
            printf("Nom : %s  \n", tab[i].nom);
            printf("Ville : %s  \n", tab[i].ville);
            printf("Code Postal : %s  \n", tab[i].cp);
            printf("Telephone : %s  \n", tab[i].tel);
            printf("Mail : %s  \n", tab[i].mail);
            printf("Metier : %s  \n", tab[i].metier);
            printf("===========================================================================\n");
        }
    }
}

int affichageMenu()
{
    int choix;

    printf("=========================MENU=========================\n");
    printf("(1). Ajouter de nouveaux clients\n");
    printf("(2). Modifier les donnees sur un client\n");
    printf("(3). Suppression d'un client\n");
    printf("(4). Afficher la liste de tous les clients\n");
    printf("(5). Filtre\n");
    printf("(6). Recherche\n");
    printf("(7). Affichage des clients ayant des donnees manquant\n");
    printf("(8). Sauvegarde des donnees\n");
    printf("(0). Quitter\n");
    printf("======================================================\n\n");

    printf("Entre votre choix s'il-vous-plait : ");
    scanf("%d", &choix);

    return choix;
}

void affichageFonction(int res_affichageMenu)
{
    Personne tab[T];

    switch(res_affichageMenu)
    {
    case 1:

        break;
    case 2:

        break;
    case 3:

        break;
    case 4:
        afficherClient(tab);
        break;
    case 5:

        break;
    case 6:
        //ok
        break;
    case 7:
        donneeManquant(tab);
        break;
    case 8:
        // ok
        break;
    case 0:
        printf("~~ A bientot ! ~~");
        break;
    default:
        printf("La valeur saisit n'est pas valide.");
        break;
    }
}

int main (void)
{
    Personne tab[T];                                          // Déclaration du tableau contenant tous les clients

    int res_affichageMenu = affichageMenu();                  // Affichage du menu

    printf("\n");

    affichageFonction(res_affichageMenu);                     // Affichage de la fonction demandee

    printf("\n");


    //afficherClient(tab);

    /*
    Personne tab[T];


    remplireTab(tab);



    tri_fusion(0,T-1, tab);

    printf("table:       \n");
        for(int c=0;c<T;c++){
                printf("%d %s \n",c, tab[c].prenom);

        }
        printf("\n");



    printf("nom:");
    puts(tab[0].nom);
    printf("nom:");
    puts(tab[49].nom);

    printf("prenom:");
    puts(tab[0].prenom);

    printf("prenom:");
    puts(tab[1].prenom);



    /*
    char str[] = "jean,bob,Lyon,69000,0755665555,p";

    tab[0] = value_to_struc(&str,tab[0]);

    puts(tab[0].metier);
    */

        return 0;
    }

