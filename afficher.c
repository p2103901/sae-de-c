#include <stdio.h>
#include <stdlib.h>

void lireFichier(void);

int main()
{
    lireFichier();

    return 0;
}

void lireFichier(void)
{
    char nomfichier[6];
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];

    FILE* entree;
    printf("nom du fichier a lire: ");
    scanf("%20s", nomfichier);
    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
    printf("%s\n", buffer);
    }

    fclose(entree);



}
