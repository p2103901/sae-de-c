#include "Donnee_Manquant.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void Donnee_Manquant(Personne *tab, int T)
{
    remplireTab(tab, T);
    int i, tranche;
    int null = strlen(tab[0].prenom);
    printf("=== Menu d'affichage des clients avec des donnees manquantes par tranches de 200 personnes ===\n");
    printf("Saisir une valeur entre 0 et 5200 : ");
    scanf("%d", &tranche);
    for(i=tranche; i<tranche+200; i++)
    {
        if(strlen(tab[i].prenom) == null || strlen(tab[i].nom) == null || strlen(tab[i].ville) == null || strlen(tab[i].cp) == null || strlen(tab[i].tel) == null || strlen(tab[i].mail) == null || strlen(tab[i].metier) == null)
        {
            printf("===========================================================================\n");
            printf("Numero : %d  \n", i);
            printf("Prenom : %s  \n", tab[i].prenom);
            printf("Nom : %s  \n", tab[i].nom);
            printf("Ville : %s  \n", tab[i].ville);
            printf("Code Postal : %s  \n", tab[i].cp);
            printf("Telephone : %s  \n", tab[i].tel);
            printf("Mail : %s  \n", tab[i].mail);
            printf("Metier : %s  \n", tab[i].metier);
            printf("===========================================================================\n");
        }
    }
}
