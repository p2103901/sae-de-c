#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Tri_Fusion.h"
#include "Struc_Pres.h"

#define min(a,b) (a<=b?a:b)





int choix_tri(int col, Personne A, Personne B){

switch (col){
case 1:
    return strcmpi(A.prenom, B.prenom);
case 2:
    return strcmpi(A.nom, B.nom);
case 3:
    return strcmpi(A.ville, B.ville);
case 4:
    return strcmpi(A.cp, B.cp);
case 5:
    return strcmpi(A.tel, B.tel);
case 6:
    return strcmpi(A.mail, B.mail);
case 7:
    return strcmpi(A.metier, B.metier);
default:
    return 0;
}


}


void fusion(int col,int deb,int mil, int fin, Personne *tab){

    int tai1 = mil - deb +1;
    int tai2 = fin - mil;

    Personne A[tai1];
    Personne B[tai2];




    for(int i =0; i<tai1;i++)
        A[i] = tab[deb +i];
    for(int j=0;j<tai2;j++)
        B[j] = tab[mil +1 +j];


     int i, j, k;

    i =0;
    j = 0;
    k = deb;


    while(i< tai1 && j < tai2)
    {

        if (choix_tri(col, A[i],B[j])<=0){

            tab[k] = A[i];
            i++;
        }
        else{

            tab[k] = B[j];

            j++;
        }
        k++;
    }
    while (i < tai1)
    {
        tab[k] = A[i];
        i++;
        k++;
    }

    while (j < tai2)
    {
        tab[k] = B[j];
        j++;
        k++;
    }




}

void tri_fusion(int col, int deb, int fin, Personne *tab){

      if (deb < fin){

        int mil = (deb+fin)/2;

        tri_fusion(col, deb, mil ,tab);

        tri_fusion(col, mil + 1, fin ,tab);

        fusion(col, deb, mil, fin, tab);



      }








}



