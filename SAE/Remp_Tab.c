#include "Struc_Pres.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//===a changer

#define min(a,b) (a<=b?a:b)

//=================

char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;

    s2=str; //this is here to avoid warning of assignment from incompatible pointer type

    s2 = strsep( &s2, "\n" ); //retire le \n

    while (s2!=0 ) {

            s1 = strsep( &s2, "," );
            //printf("%s\n",p.cp);

            switch (count){
                case 1:
                    strcpy(p.prenom ,s1);
                    break;
                case 2:
                    strcpy(p.nom ,s1);
                    break;
                case 3:
                    strcpy(p.ville ,s1);
                    break;
                case 4:
                    strcpy(p.cp ,s1);
                    break;
                case 5:
                    strcpy(p.tel ,s1);
                    break;
                case 6:
                    strcpy(p.mail ,s1);
                    break;
                case 7:
                    strcpy(p.metier ,s1);
                    break;
            }

            count +=1;
    }


    return p;
}

void remplireTab(Personne *tab, int T){
    char nomfichier[8] = "an.csv";
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];
    int count = 0;

    FILE* entree;
    //printf("nom du fichier a lire: ");
    //scanf("%20s", nomfichier);

    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
            //puts(tab[count].metier);
            tab[count] = value_to_struc(&buffer,tab[count]);
            //puts(tab[count].metier);
            count++;
            if (count>T-1){
                fclose(entree);
            }

    }

    fclose(entree);



}
