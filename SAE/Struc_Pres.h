#ifndef Struc_Pres_h_
#define Struc_Pres_h_



struct personne {
  char prenom[21];
  char nom[27];
  char ville[23];
  char cp[6];
  char tel[15];
  char mail[47];
  char metier[18];
};

typedef struct personne Personne;


#endif
