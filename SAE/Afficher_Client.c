#include "Afficher_Client.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void Afficher_Client(Personne *tab, int T)
{
    //remplireTab(tab, T);
    int i, tranche, choix_tri, choix_type_tri;
    printf("=== Menu d'affichage des clients par tranches de 200 personnes ===\n");
    printf("Souhaitez vous un tri sur l'affichage ? (0/1) : ");
    scanf("%d", &choix_tri);
    while(choix_tri<0 && choix_tri>0)
    {
        scanf("%d", &choix_tri);
    }
    if(choix_tri==1)
    {
        printf("(1) Tri sur le prenom.\n");
        printf("(2) Tri sur le nom.\n");
        printf("(3) Tri sur la ville.\n");
        printf("(4) Tri sur le code postal.\n");
        printf("(5) Tri sur le numero de telephone.\n");
        printf("(6) Tri sur le mail.\n");
        printf("(7) Tri sur le metier.\n");
        printf("Choix : ");
        scanf("%d", &choix_type_tri);

        clock_t temp_start, temp_end;
        unsigned int temp_cpu;
        temp_start = clock();

        tri_fusion(choix_type_tri, 0, T, tab);
        temp_end = clock();
        temp_cpu = (temp_end - temp_start) ;

        printf("%x ms \n",temp_cpu);


    }
    printf("Saisir une valeur entre 0 et 5200 : ");
    scanf("%d", &tranche);

    for(i=tranche;i<tranche+200;i++)

    {
        if (i< T){
        printf("===========================================================================\n");
        printf("Numero : %d  \n", i);
        printf("Prenom : %s  \n", tab[i].prenom);
        printf("Nom : %s  \n", tab[i].nom);
        printf("Ville : %s  \n", tab[i].ville);
        printf("Code Postal : %s  \n", tab[i].cp);
        printf("Telephone : %s  \n", tab[i].tel);
        printf("Mail : %s  \n", tab[i].mail);
        printf("Metier : %s  \n", tab[i].metier);
        printf("===========================================================================\n");
    }}
}
