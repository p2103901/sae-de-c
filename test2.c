#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#define min(a,b) (a<=b?a:b)

#define T 5001

struct personne {
  char prenom[21];
  char nom[27];
  char ville[23];
  char cp[5];
  char tel[14];
  char mail[47];
  char metier[18];
};

typedef struct personne Personne;


char strstrcol(int col,char *str, Personne A);
void affiche_list(Personne *tab, int taille, int *tab_a_aff, int taille_a_aff);
void affiche_list2(Personne *tab, int taille, int **tab_a_aff, int *taille_a_aff);
char strcmpicharpers(int col,char *str,Personne A);
char *strsep(char **stringp, const char *delim);
Personne value_to_struc(char *str,Personne p);
void remplireTab(Personne *tab);
void tri_fusion(int col, int deb, int fin, Personne *tab);
void fusion(int col,int deb,int mil, int fin, Personne *tab);
int choix_col(int col, Personne A, Personne B);
int recherche_dicho(int col,char *str, Personne *tab, int taille);
void filtre(int col,Personne *tab, int taille, char *str,int *tab_res, int *taille_res );

char strstrcol(int col,char *str, Personne A){
    switch (col){
case 1:

    return strcmpi(A.prenom,str);
case 2:

    return strcmpi(str, A.nom);
case 3:
    return strcmpi(str, A.ville);
case 4:
    return strcmpi(str, A.cp);
case 5:
    return strcmpi(str, A.tel);
case 6:
    return strcmpi(str, A.mail);
case 7:
    return strcmpi(str, A.metier);
default:
    return 0;
}

}

char strcmpicharpers(int col,char *str,Personne A){
    switch (col){
case 1:

    return strcmpi(str, A.prenom);
case 2:

    return strcmpi(str, A.nom);
case 3:
    return strcmpi(str, A.ville);
case 4:
    return strcmpi(str, A.cp);
case 5:
    return strcmpi(str, A.tel);
case 6:
    return strcmpi(str, A.mail);
case 7:
    return strcmpi(str, A.metier);
default:
    return 0;
}
              }

char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        //printf("%d ",min(strcspn(*stringp, delim),strcspn(*stringp, "\n")));
        *stringp += min(strcspn(*stringp, delim),strcspn(*stringp, "\n"));
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

Personne value_to_struc(char *str,Personne p){

    char *s1;
    char *s2;
    int count =1 ;

    s2=str; //this is here to avoid warning of assignment from incompatible pointer type

    s2 = strsep( &s2, "\n" ); //retire le \n

    while (s2!=0 ) {

            s1 = strsep( &s2, "," );

            switch (count){
                case 1:
                    strcpy(p.prenom ,s1);
                    break;
                case 2:
                    strcpy(p.nom ,s1);
                    break;
                case 3:
                    strcpy(p.ville ,s1);
                    break;
                case 4:
                    strcpy(p.cp ,s1);
                    break;
                case 5:
                    strcpy(p.tel ,s1);
                    break;
                case 6:
                    strcpy(p.mail ,s1);
                    break;
                case 7:
                    strcpy(p.metier ,s1);
                    break;
            }

            count +=1;
    }


    return p;
}

void remplireTab(Personne *tab){
    char nomfichier[8] = "an.csv";
    int taillebuffer = 50+50+50+5+70+50;
    char buffer[taillebuffer];
    int count = 0;

    FILE* entree;
    //printf("nom du fichier a lire: ");
    //scanf("%20s", nomfichier);

    entree = fopen(nomfichier, "r"); /* ouverture en lecture */


    if(entree == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier."); /* message d'erreur si le fichier n'existe pas */
        exit(1);
    }


    while(fgets(buffer, taillebuffer, entree)) {
            //puts(tab[count].metier);
            tab[count] = value_to_struc(&buffer,tab[count]);
            //puts(tab[count].metier);
            count++;
            if (count>T-1){
                fclose(entree);
            }

    }

    fclose(entree);



}

int choix_col(int col, Personne A, Personne B){

switch (col){
case 1:

    return strcmpi(A.prenom, B.prenom);
case 2:

    return strcmpi(A.nom, B.nom);
case 3:
    return strcmpi(A.ville, B.ville);
case 4:
    return strcmpi(A.cp, B.cp);
case 5:
    return strcmpi(A.tel, B.tel);
case 6:
    return strcmpi(A.mail, B.mail);
case 7:
    return strcmpi(A.metier, B.metier);
default:
    return 0;
}


}

void fusion(int col,int deb,int mil, int fin, Personne *tab){

    int tai1 = mil - deb +1;
    int tai2 = fin - mil;

    Personne A[tai1];
    Personne B[tai2];




    for(int i =0; i<tai1;i++)
        A[i] = tab[deb +i];
    for(int j=0;j<tai2;j++)
        B[j] = tab[mil +1 +j];


     int i, j, k;

    i =0;
    j = 0;
    k = deb;


    while(i< tai1 && j < tai2)
    {

        if (choix_col(col, A[i],B[j])<=0){

            tab[k] = A[i];
            i++;
        }
        else{

            tab[k] = B[j];

            j++;
        }
        k++;
    }
    while (i < tai1)
    {
        tab[k] = A[i];
        i++;
        k++;
    }

    while (j < tai2)
    {
        tab[k] = B[j];
        j++;
        k++;
    }




}

void tri_fusion(int col, int deb, int fin, Personne *tab){

      if (deb < fin){

        int mil = (deb+fin)/2;

        tri_fusion(col, deb, mil ,tab);

        tri_fusion(col, mil + 1, fin ,tab);

        fusion(col, deb, mil, fin, tab);



      }






}

int recherche_dicho(int col,char *str, Personne *tab, int taille){

    int deb = 0;
    int fin = taille-1;
    int m;

    while (deb <=fin){
        m = (deb +fin)/2;

        if (strcmpicharpers(col,str, tab[m])==0){
            return m;
        }else if(strcmpicharpers(col,str, tab[m])>0){

            deb = m + 1;
        }else{

            fin = m-1;
        }
    }
    return -1;


}


void affiche_list(Personne *tab, int taille, int *tab_a_aff, int taille_a_aff){
    int c;
    for(int i =0; i<taille_a_aff; i++){
        c = tab_a_aff[i];

        printf("===========================================================================\n");
                printf("Numero : %d  \n", c);
                printf("Prenom : %s  \n", tab[c].prenom);
                printf("Nom : %s  \n", tab[c].nom);
                printf("Ville : %s  \n", tab[c].ville);
                printf("Code Postal : %s  \n", tab[c].cp);
                printf("Telephone : %s  \n", tab[c].tel);
                printf("Mail : %s  \n", tab[c].mail);
                printf("Metier : %s  \n", tab[c].metier);
                printf("===========================================================================\n");
        }


    }
void affiche_list2(Personne *tab, int taille, int **tab_a_aff, int *taille_a_aff){
    /*printf("__%d\n",(tab_a_aff));
    printf("__%d\n",*(tab_a_aff+1));
    printf("__%d\n",*(tab_a_aff+2));
    printf("__%d\n",*(tab_a_aff+3));
    printf("__%d\n",*(tab_a_aff+4));*/

    int c;
    for(int i =0; i<taille_a_aff; i++){
        c = *(tab_a_aff+i+1);

        printf("===========================================================================\n");
                printf("Numero : %d  \n", c);
                printf("Prenom : %s  \n", tab[c].prenom);
                printf("Nom : %s  \n", tab[c].nom);
                printf("Ville : %s  \n", tab[c].ville);
                printf("Code Postal : %s  \n", tab[c].cp);
                printf("Telephone : %s  \n", tab[c].tel);
                printf("Mail : %s  \n", tab[c].mail);
                printf("Metier : %s  \n", tab[c].metier);
                printf("===========================================================================\n");
        }


    }


void filtre(int col, Personne *tab, int taille, char *str,int *tab_res, int *taille_res ){
    int c =0;
    int buffer =10;
    tab_res = (int*)malloc(10*sizeof(int));

    //tab_res = realloc(tab_res,1000);


    for(int i =0;i< taille;i++){

    if (strstr(tab[i].prenom,str) != NULL){
            c++;
            printf("%d\n",c);

{           if(c* sizeof(int)> buffer){

            tab_res = realloc(tab_res,c*c);
            buffer = c*c;}

}            *(tab_res+c) =i;
    }











}
printf("end");

* taille_res =c;

}



int main ()
{
    int tail_aff;

    int *to_aff;



    Personne tab[T];
    remplireTab(tab);

    filtre(1,tab,T,"ol",&to_aff, &tail_aff);


    printf("%d res\n",tail_aff);

    for(int z=0;z<tail_aff;z++){

        printf("%d",*(to_aff+z));
    }

    /*
    int *to_aff = -1;
    int tail_aff =5000;



    to_aff = (int*)malloc(tail_aff*sizeof(int));

    Personne tab[T];


    remplireTab(tab);

    tri_fusion(5 ,1,T-1, tab);

    int x = recherche_dicho(5,"04.61.30.65.53",tab,T);

    //affiche_list(tab,T,to_aff,2);

    filtre(1,tab,T,"ol",&to_aff, &tail_aff);

    printf("__%d",tail_aff);

    affiche_list2(tab,T,&to_aff,tail_aff);

    */






printf("Succes");
        return 0;
    }


