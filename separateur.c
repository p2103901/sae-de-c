#include <stdio.h>
#include <string.h>


char *strsep(char **stringp, const char *delim);
char *value_at_index(char *str,int index);



char *strsep(char **stringp, const char *delim) {
    char *rv = *stringp;
    if (rv) {
        *stringp += strcspn(*stringp, delim);
        if (**stringp)
            *(*stringp)++ = '\0';
        else
            *stringp = 0; }
    return rv;
}

char *value_at_index(char *str,int index){
    /* basé sur un code de xerostomus https://stackoverflow.com/users/3586932/xerostomus*/

    char *s1;
    char *s2;
    int count =1 ;
    s2=str; //this is here to avoid warning of assignment from incompatible pointer type
    while (s2!=0 ) {
            while( *s2 == ' ' || *s2 == '\t' )  s2++;
            s1 = strsep( &s2, "," );
            if (count == index){
                return s1;

            }else{
                count +=1;
            }

        }
        return 0;


}


int main ()
{


        char str[] = "1,bonjour,3,5,9,064854";
        puts(value_at_index(&str,2));
        return 0;
    }
